package org.example;

import lombok.extern.slf4j.Slf4j;
import org.example.exceptions.DataNotFoundException;
import ru.mos.etp.model.CustomWebService2;
import ru.mos.etp.model.CustomWebServiceImpl;

import javax.xml.bind.DatatypeConverter;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

@Slf4j
public class SoapServiceBuilder {

    private SoapServiceBuilder() {}

    private static CustomWebServiceImpl customWebService;

    public static CustomWebServiceImpl build(Properties inputData) throws DataNotFoundException {
        if (customWebService==null) {
            customWebService = new CustomWebService2().getCustomWebServiceImplPort();

            Map<String, List<String>> requestHeaders = new HashMap<>();
            if (inputData.get("LOGIN") == null || inputData.get("PASSWORD") == null) {
                throw new DataNotFoundException("login or password isn't set");
            }
            String creds = inputData.get("LOGIN").toString() + ":" + inputData.get("PASSWORD").toString();

            creds = DatatypeConverter.printBase64Binary(creds.getBytes());
            requestHeaders.put("Authorization", Collections.singletonList("Basic " + (creds)));
            BindingProvider bindingProvider = (BindingProvider) customWebService;
            bindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, requestHeaders);
        }
        return customWebService;
    }
}
