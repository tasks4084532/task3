package org.example;

import lombok.Getter;
import lombok.Setter;
import org.example.consts.UsefulFields;
import ru.mos.etp.model.Property;

import java.util.List;
import java.util.Optional;

@Getter
@Setter
public class Metadata {
    private Boolean archived;
    private int asgufCode;
    private float contentSize;
    private String corpId;
    private String id;
    private String lastModifier;
    private String mimeType;
    private String name;
    private String owners;
    private String ssoid;


    public Metadata getDataByProps(List<Property> properties) {
        for (Property property : properties) {
            Optional<UsefulFields> field = UsefulFields.exist(property.getName());
            field.ifPresent(usefulFields -> usefulFields.setDataToMetadata(this, property));
        }
        return this;
    }
}
