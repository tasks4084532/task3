package org.example;

import lombok.extern.slf4j.Slf4j;
import org.example.exceptions.DataNotFoundException;
import org.example.requests.Requests;
import ru.mos.etp.model.CustomWebServiceImpl;
import ru.mos.etp.model.ErrorMessageException_Exception;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.util.Map;
import java.util.Properties;

@Slf4j
public class Main {
    public static void main(String[] args) throws IOException, DataNotFoundException {
        Properties inputData = new Properties();
        FileInputStream inputFile = new FileInputStream(".\\src\\main\\resources\\application.properties");
        inputData.load(inputFile);
        inputFile.close();
        if (inputData.get("filePath") == null) {
            throw new DataNotFoundException("path to the directory where result should be saved isn't set");
        }
        String pathResultFile = inputData.get("filePath").toString();

        if (inputData.get("documentId") == null) {
            throw new DataNotFoundException("documentId isn't set");
        }
        String documentId = inputData.get("documentId").toString();
        log.info("--START-- pathResultFile = " + pathResultFile + "; documentId = " + documentId);

        CustomWebServiceImpl soapService = SoapServiceBuilder.build(inputData);

        try {
            Requests requests = new Requests();
            requests.getMeta(soapService, documentId, pathResultFile);
            requests.getDoc(soapService, documentId, pathResultFile);
        } catch (ErrorMessageException_Exception ex) {
            log.warn(ex.getMessage());
        } catch (FileSystemException ex) {
            log.error(ex.getMessage());
        }
    }
}
