package org.example.requests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import lombok.extern.slf4j.Slf4j;
import org.example.Metadata;
import ru.mos.etp.model.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

@Slf4j
public class Requests {
    public void getMeta(CustomWebServiceImpl soapService, String documentId, String pathResultFile) throws ErrorMessageException_Exception, IOException {

        log.info("Sending GetDocumentProperties request");
        GetDocumentPropertiesResponse.Properties properties = soapService.getDocumentProperties(documentId);
        List<Property> props = properties.getProperty();
        log.info("Received metadata of document " + documentId);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(PropertyNamingStrategies.UPPER_CAMEL_CASE);

        Metadata metadata = new Metadata().getDataByProps(props);

        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        writer.writeValue(new File(pathResultFile + "metadata.json"), metadata);
        log.info("Saved metadata of document " + documentId + " to " + pathResultFile + "metadata.json");
    }

    public void getDoc(CustomWebServiceImpl soapService, String documentId, String pathResultFile) throws ErrorMessageException_Exception, IOException {
        GetDocumentRequest documentRequest = new GetDocumentRequest();
        documentRequest.setDocumentId(documentId);

        log.info("Sending GetDocument request");
        byte[] document = soapService.getDocumentData(documentRequest);
        log.info("Received document " + documentId);
        Files.write(new File(pathResultFile + "document.pdf").toPath(), document);
        log.info("Saved document " + documentId + " to " + pathResultFile + "document.pdf");
    }
}
