package org.example.consts;

import org.example.Metadata;
import ru.mos.etp.model.Property;

import java.util.Arrays;
import java.util.Optional;

public enum UsefulFields {

    ARCHIVED("Archived") {
        @Override
        public void setDataToMetadata(Metadata metadata, Property property) {
            metadata.setArchived(Boolean.parseBoolean(property.getValue()));
        }
    },
    ASGUFCODE("ASGUF_Code"){
        @Override
        public void setDataToMetadata(Metadata metadata, Property property) {
            metadata.setAsgufCode(Integer.parseInt(property.getValue()));
        }
    },
    CONTENT_SIZE("ContentSize"){
        @Override
        public void setDataToMetadata(Metadata metadata, Property property) {
            metadata.setContentSize(Float.parseFloat(property.getValue()));
        }
    },
    CORP_ID("corpid"){
        @Override
        public void setDataToMetadata(Metadata metadata, Property property) {
            metadata.setCorpId(property.getValue());
        }
    },
    ID("Id"){
        @Override
        public void setDataToMetadata(Metadata metadata, Property property) {
            metadata.setId(property.getValue());
        }
    },
    LAST_MODIFIER("LastModifier"){
        @Override
        public void setDataToMetadata(Metadata metadata, Property property) {
            metadata.setLastModifier(property.getValue());
        }
    },
    MIME_TYPE("MimeType"){
        @Override
        public void setDataToMetadata(Metadata metadata, Property property) {
            metadata.setMimeType(property.getValue());
        }
    },
    NAME("Name"){
        @Override
        public void setDataToMetadata(Metadata metadata, Property property) {
            metadata.setName(property.getValue());
        }
    },
    OWNERS("Owner"){
        @Override
        public void setDataToMetadata(Metadata metadata, Property property) {
            metadata.setOwners(property.getValue());
        }
    },
    SSOID("ssoid"){
        @Override
        public void setDataToMetadata(Metadata metadata, Property property) {
            metadata.setSsoid(property.getValue());
        }
    };

    private final String title;

    UsefulFields(String title) {
        this.title = title;
    }

    public abstract void setDataToMetadata(Metadata metadata, Property property);


    public static Optional<UsefulFields> exist(String targetString) {

        return Arrays
                .stream(UsefulFields.values())
                .filter(el -> el.title.equals(targetString))
                .findFirst();
    }
}