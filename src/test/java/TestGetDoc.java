import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.Main;
import org.example.exceptions.DataNotFoundException;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestGetDoc {
    @org.junit.Test
    public void Test() throws IOException, DataNotFoundException {
        String[] wrongInput = {"C:/Users/akast/OneDrive/Рабочий стол/Java/task3/", "B77322C0-F32F-4BEF-B345-BB9D0754D5EF"};
        Main.main(wrongInput);

        String[] rightInput = {"C:/Users/akast/OneDrive/Рабочий стол/Java/task3/", "B77322C0-F32F-4BEF-B345-BB9D0754D5EF"};
        Main.main(rightInput);
        File filePdf = new File(rightInput[0] + "document.pdf");
        assertTrue(filePdf.exists() && !filePdf.isDirectory());
        File fileJson = new File(rightInput[0] + "metadata.json");
        assertTrue(fileJson.exists() && !fileJson.isDirectory());

        //check downloaded document id
        File file = new File(rightInput[0] + "metadata.json");
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> metadata = mapper.readValue(file, Map.class);
        assertEquals("{" + rightInput[1] + "}", metadata.get("Id"));
    }
}
